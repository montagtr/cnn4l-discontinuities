import torch
from torch.utils.data import Dataset
import numpy as np


class MyDataset(Dataset):

    def __init__(self, pre, post, height_correlation, width_correlation, correlation_window_size, stride, correction_maps=None, correction=False):
        self.pre = pre
        self.post = post
        self.height_correlation = height_correlation
        self.width_correlation = width_correlation
        self.correlation_window_size = correlation_window_size
        self.stride = stride
        self.correction_maps = correction_maps
        self.correction = correction
        
    def __len__(self):
        return self.height_correlation * self.width_correlation

    def __getitem__(self, idx):
        # coordinates of the next windows pair
        top, left = (idx // self.width_correlation) * self.stride, (idx % self.width_correlation) * self.stride 
        
        if self.correction:
            corr_h = self.correction_maps[1][idx // self.width_correlation, idx % self.width_correlation]
            corr_w = self.correction_maps[1][idx // self.width_correlation, idx % self.width_correlation]
            if not np.isnan(corr_h):
                corr_top = top + int(corr_h)
                if 0 < corr_top < self.pre.shape[0]:
                    top = corr_top
            if not np.isnan(corr_w):
                corr_left = left + int(corr_w)
                if 0 < corr_left < self.pre.shape[1]:
                    left = corr_left
        
        bottom, right = top + self.correlation_window_size, left + self.correlation_window_size

        # Selection of the windows pair
        window_pre = torch.tensor(np.asarray(self.pre[top:bottom, left:right]).astype(np.float32), dtype=torch.float32)
        window_post = torch.tensor(np.asarray(self.post[top:bottom, left:right]).astype(np.float32), dtype=torch.float32)
        
        # Standardization
        window_pre = (window_pre - window_pre.mean()) / window_pre.std()
        window_post = (window_post - window_post.mean()) / window_post.std()

        # Stack pre- and post-windows
        window_pre_post = torch.stack((window_pre, window_post), dim=0)

        return window_pre_post
