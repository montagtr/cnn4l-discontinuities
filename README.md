# cnn4l-discontinuities - Pytorch Implementation

cnn4l-discontinuities performs sub-pixel correlation of optical satellite images for the retrieval of ground displacement, designed to mitigate bias around fault ruptures.

## Prerequisites

Before you begin, make sure you have Conda installed on your system. You can download and install Conda by following the instructions on the [official Conda website](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html).

### Step 1: Create a Conda Environment

```
conda create --name cnn4l-discontinuities python=3.8 cudatoolkit=11.8
conda activate cnn4l-discontinuities
```

### Step 2: Install Required Libraries

You can install the required libraries using Conda and pip:

```
conda install numpy tqdm shapely 
pip install rasterio torch
```

## Running pipeline with pre-trained models

The images pairs should be in the data repository

```
python3 inference.py
```

## How to cite this material

10.5281/zenodo.12170087

## Lastest version available : 

https://gricad-gitlab.univ-grenoble-alpes.fr/montagtr/cnn4l-discontinuities.git
