#!/bin/bash

#SBATCH --job-name=inference # nom du job
#SBATCH --output=inference.%j.out # fichier de sortie (%j = job ID)
#SBATCH --error=inference.%j.err # fichier d’erreur (%j = job ID)
#SBATCH --ntasks-per-node=1 # nombre de tache MPI par noeud (= nombre de GPU par noeud)
#SBATCH --cpus-per-task=10 # nombre de CPU par tache
#SBATCH --gres=gpu:1 # reserver 1 GPU par noeud
#SBATCH --time=10:00:00 # temps maximal d’allocation "(HH:MM:SS)"
#SBATCH --hint=nomultithread # desactiver l’hyperthreading
#SBATCH --account=dal@v100
#SBATCH -C v100-32g

module purge # nettoyer les modules herites par defaut
module load pytorch-gpu/py3/1.11.0 # charger les modules

set -x # activer l’echo des commandes
srun python -u /gpfswork/rech/dal/ukp66af/cnn4l-discontinuities/inference.py # executer son script
