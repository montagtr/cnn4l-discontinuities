import torch
import torch.nn as nn
import torch.nn.functional as F


class Cnn4l(nn.Module):
    def __init__(self):
        super().__init__()
        #  (((W - K + 2P)/S) + 1)
        self.conv1 = torch.nn.Conv2d(2, 64, kernel_size=3, stride=1)
        self.conv2 = torch.nn.Conv2d(64, 128, kernel_size=3, stride=1)
        self.conv3 = torch.nn.Conv2d(128, 256, kernel_size=3, stride=1)
        self.conv4 = torch.nn.Conv2d(256, 256, kernel_size=3, stride=1)

        self.fc1 = torch.nn.Linear(8*8*256, 64)  # Placeholder for dynamically calculated input size
        self.fc2 = torch.nn.Linear(64, 2)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = F.relu(self.conv3(x))
        x = F.relu(self.conv4(x))
        x = x.view(-1, 8*8*256)  # Flatten the tensor
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return x
        
        
class Cnn3lpixel(nn.Module):
    def __init__(self):
        super().__init__()
        #  (((W - K + 2P)/S) + 1)
        self.conv1 = torch.nn.Conv2d(2, 16, kernel_size=5, stride=1)     # 32*32*2 -> 28*28*16
        self.conv2 = torch.nn.Conv2d(16, 32, kernel_size=5, stride=1)    # 28*28*16 -> 24*24*32
        self.conv3 = torch.nn.Conv2d(32, 64, kernel_size=5, stride=1)   # 24*24*32 -> 20*20*64

        self.fc1 = torch.nn.Linear(20*20*64, 64)                          # INPUT 20*20*64 features, OUTPUT 64 features
        self.fc2 = torch.nn.Linear(64, 2)                                # INPUT 64 features, OUTPUT 2 features

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = F.relu(self.conv3(x))
        x = x.view(-1, 20*20*64)
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return x
