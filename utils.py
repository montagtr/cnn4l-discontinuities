import os
import numpy as np
from tqdm import tqdm
import rasterio as rio
from shapely.geometry import box

import torch

from data_process import MyDataset 


def overlap_two_rasters(raster1_path, raster2_path, folder_saving):
    """
    This function takes paths to two raster files, computes their intersection, 
    extracts overlapping regions, and saves the result as a new raster file. It 
    returns the overlapping regions, CRS, and transformation details.
                             
    :param raster1_path:     path to the first raster
    :param raster2_path:     path to the second raster
    :param folder_saving:    folder where to save the raster containing overlap
    :return: 
    output-1:                array containing the overlap area of the first raster
    output-2:                array containing the overlap area of the first raster
    output-3:                coordinate reference system of the first raster
    output-4:                contains the affine based coordinates in the corresponding CRS
    output-5:                resolution of the first raster
    """
    with rio.open(raster1_path) as ras1, rio.open(raster2_path) as ras2:
        if ras1.transform == rio.Affine(1, 0.0, 0.0, 0.0, 1, 0.0):
            return ras1.read(1), ras2.read(1), None, None, 1
        else:
            ext1 = box(*ras1.bounds)
            ext2 = box(*ras2.bounds)
            intersection = ext1.intersection(ext2)
            transform_overlap = rio.Affine(ras1.res[0], 0.0, min(intersection.bounds[0], intersection.bounds[2]),
                                           0.00, -ras1.res[1], max(intersection.bounds[1], intersection.bounds[3]))

            win1 = rio.windows.from_bounds(*intersection.bounds, ras1.transform)
            win2 = rio.windows.from_bounds(*intersection.bounds, ras2.transform)
            overlap_pre = ras1.read(1, window=win1).astype(np.float32)
            overlap_post = ras2.read(1, window=win2).astype(np.float32)
            crs = ras1.crs

            overlap_pre[(overlap_pre == 0.)] = np.nan
            overlap_post[(overlap_post == 0.)] = np.nan

            stacked_overlap_path = os.path.join(folder_saving,'overlap.tif')

            with rio.open(
                    stacked_overlap_path,
                    mode="w",
                    driver="GTiff",
                    height=overlap_pre.shape[0],
                    width=overlap_pre.shape[1],
                    count=2,
                    dtype=overlap_pre.dtype,
                    crs=crs,
                    transform=transform_overlap,
            ) as new_dataset:
                new_dataset.write(overlap_pre, 1)
                new_dataset.write(overlap_post, 2)
    print(f"\nOverlap of the two rasters is stored in {folder_saving}")

    return overlap_pre, overlap_post, crs, transform_overlap, abs(ras1.res[0])


def rastering_displacement_maps(disp_ew, disp_ns, crs, transform_overlap, stacked_maps_path):
    """
    Given east-west and north-south displacement maps, CRS, and transformation details, this 
    function creates a new raster file combining the displacement maps and saves it at the 
    specified path.
    
    :param disp_ew:                  displacement map array EW component
    :param disp_ns:                  displacement map array NS component
    :param crs:                      coordinate reference system to raster
    :param transform_overlap:        affine based coordinates in the corresponding CRS to raster
    :stacked_maps_path:              path where to save the raster
    """
    with rio.open(
            stacked_maps_path,
            mode="w",
            driver="GTiff",
            height=disp_ew.shape[0],
            width=disp_ew.shape[1],
            count=2,
            dtype=disp_ew.dtype,
            crs=crs,
            transform=transform_overlap,
    ) as new_dataset:
        new_dataset.write(disp_ew, 1)
        new_dataset.write(disp_ns, 2)
    pass


def correlation(pre, post, model, device, window_size_correlation, window_size_correction, stride, batch_size, correction_maps=None, correction=False):
    """
    This function performs correlation between pre and post-event imagery using a 
    PyTorch model. It initializes a custom dataset, loads data using a DataLoader, 
    and computes correlation matrices for east-west and north-south displacements.
    
    :param pre:                        array of the first image
    :param post:                       array of the second image
    :param model:                      model that performs the correlation
    :param device:                     device to perform the computation inside the model
    :param window_size:                size of the correlation window
    :param stride:                     stride of the correlation
    :param batch_size:                 batch size for the model
    :return: 
    output-1:                          displacement map tensor EW component
    output-2:                          displacement map tensor NS component
    """
    height_correlation = int((pre.shape[0]-window_size_correction)/stride) + 1
    width_correlation = int((pre.shape[1]-window_size_correction)/stride) + 1        
    print(f'\nBatch size: {batch_size}')               
    
    inf_set = MyDataset(pre, post, height_correlation, width_correlation, window_size_correlation, stride, correction_maps=correction_maps, correction=correction)
    
    inf_loader = torch.utils.data.DataLoader(dataset=inf_set,                                                                                               
                                             batch_size=batch_size,                                                                                    
                                             shuffle=False,                                                                                          
                                             num_workers=10,                                                                                  
                                             persistent_workers=True,                                                                    
                                             pin_memory=True,                                                                                    
                                             prefetch_factor=3,                                                                          
                                             drop_last=False)
    
    print('{} samples'.format(len(inf_set)))
    
    N_batch = len(inf_loader)

    matrix_ew =torch.tensor([]).to(device)
    matrix_ns = torch.tensor([]).to(device)
    
    for i, batch in tqdm(enumerate(inf_loader), desc=f"Progression on {N_batch}it "):
        input = batch.to(device, non_blocking=True)
        with torch.no_grad():                                                                                                 
            outputs = model(input)

        matrix_ew = torch.cat((matrix_ew, outputs[:,0]))
        matrix_ns = torch.cat((matrix_ns, outputs[:,1]))

    matrix_ew = matrix_ew.view(height_correlation, width_correlation).cpu()
    matrix_ns = matrix_ns.view(height_correlation, width_correlation).cpu()
    if correction:
        matrix_ew += correction_maps[0]
        matrix_ns += correction_maps[1]
    return [matrix_ew, matrix_ns]


def padding_nan(correlation_map_ew, correlation_map_ns, h, b, g, d):
    """
    Given correlation maps for east-west and north-south displacements, 
    this function pads them with NaN values based on specified margins 
    (h, b, g, d).
    
    :param correlation_map_ew:        displacement map array EW component
    :param correlation_map_ns:        displacement map array EW component
    :param h:                         top edge amount of padding
    :param b:                         bottom edge amount of padding
    :param g:                         left edge amount of padding
    :param d:                         right edge amount of padding

    :return: 
    output-1:                         displacement map tensor EW component padded
    output-2:                         displacement map tensor NS component padded
    """
    correlation_map_ns = np.pad(correlation_map_ns, [(h, b), (g, d)],
                                'constant', constant_values=(np.nan, np.nan))
    correlation_map_ew = np.pad(correlation_map_ew, [(h, b), (g, d)],
                                'constant', constant_values=(np.nan, np.nan))
    return correlation_map_ew, correlation_map_ns


def resize(correlation_map_ew, correlation_map_ns, stride):
    """
    This function resizes correlation maps by replicating values 
    in each direction based on the specified stride.
    
    :param correlation_map_ew:         displacement map array EW component
    :param correlation_map_ns:         displacement map array EW component
    :param stride:                     stride of the correlation run

    :return: 
    output-1:                          displacement map tensor EW component resized
    output-2:                          displacement map tensor NS component resized
    """
    return np.kron(correlation_map_ew, np.ones((stride, stride))), np.kron(correlation_map_ns, np.ones((stride, stride)))
