# Import necessary libraries
import os
import datetime
import time
import argparse
import json
import numpy as np
import rasterio as rio

import torch

# Import custom modules
import utils
from train.models import models


# Check if CUDA (GPU) is available
use_cuda = torch.cuda.is_available()
device = torch.device("cuda" if use_cuda else 'cpu')
print(f"Computation device: {device}")

# Arguments of the run
parser = argparse.ArgumentParser(description="Pipeline arguments")
parser.add_argument("--seed", type=int, default=1, help="Seed value")
parser.add_argument("--correction", type=bool, default=True, help="Set False if you do not need a pixel registration before sub-pixel registration")
parser.add_argument("--window-size-sub-pixel", type=int, default=16, help="Window size for sub-pixel estimation")
parser.add_argument("--window-size-pixel", type=int, default=32, help="Window size for pixel estimation")
parser.add_argument("--stride", type=int, default=1, help="Stride value")
parser.add_argument("--batch-size", type=int, default=1024, help="batch size value")
parser.add_argument("--model-sub-pixel-path", type=str, default="./train/models/sub_pixel_model.pth", help="Path to the sub-pixel model")
parser.add_argument("--model-pixel-path", type=str, default="./train/models/pixel_model.pth", help="Path to the pixel model")
parser.add_argument("--pre-image", type=str, default="/lustre/fsn1/projects/rech/dal/ukp66af/ridgecrest/area_7_pre_50cm_crop4096.tif", help="Path to pre-image")
parser.add_argument("--post-image", type=str, default="/lustre/fsn1/projects/rech/dal/ukp66af/ridgecrest/area_7_post_50cm_crop4096.tif", help="Path to post-image")
parser.add_argument("--compute-overlap", type=bool, default=0)

args = parser.parse_args()

# Define the path for saving results and save the parameters of the run

PATH_RESULTS = "./results/"
if os.path.exists(PATH_RESULTS) is not True:
    os.mkdir(PATH_RESULTS)
    
## Current date and time
now = datetime.datetime.now()
date = now.strftime("%Y_%m_%d")
date_with_hr_sec = now.strftime("%Y_%m_%d_%H_%M_%S")
folder_saving = os.path.join(PATH_RESULTS, date_with_hr_sec)
os.mkdir(folder_saving)

## Path to the displacement maps repository
displacement_maps_path = os.path.join(folder_saving, 'displacement_maps')
os.mkdir(displacement_maps_path)

## Path to the stacked (EW and NS) displacement maps
stacked_maps_path = os.path.join(displacement_maps_path, 'stacked_displacement_maps.tif')

## Save the arguments to a file, using JSON format
args_dict = vars(args)
with open(os.path.join(folder_saving, 'args.json'), 'w') as file:
    json.dump(args_dict, file)

# Start the timer
start = time.time()
    
# Initialize and load the sub-pixel model
model_sub_pixel = models.Cnn4l()
model_sub_pixel.load_state_dict(torch.load(args.model_sub_pixel_path, map_location=torch.device(device)))
model_sub_pixel.eval()
model_sub_pixel.to(device)

# Initialize and load the pixel model
if args.correction:
    model_pixel = models.Cnn3lpixel()
    model_pixel.load_state_dict(torch.load(args.model_pixel_path, map_location=torch.device(device)))
    model_pixel.eval()
    model_pixel.to(device)

# If compute-overlap is set to True, the displacement maps estimation will be performed only on the overlapping parts of the input pair, and a raster 
# containing pre- and post-image overlap will be created and saved 
if args.compute_overlap:
    pre, post, crs, transform, resolution = utils.overlap_two_rasters(args.pre_image, args.post_image, folder_saving)

# If compute-overlap is set to False, the displacement maps estimation will be performed directly on the given input pair
else:
    with rio.open(args.pre_image) as pre_raster, rio.open(args.post_image) as post_raster:
        pre, post, crs, transform, resolution = pre_raster.read(1), post_raster.read(1), pre_raster.crs, pre_raster.transform, abs(pre_raster.res[0])

# Perform computation of displacement maps
print("\nStarting the computation...")
start_computation = time.time()
print(args.correction)
if args.correction:
    print("\nStarting the pixel registration...")
    # Initial pixel registration 
    out_correction = utils.correlation(pre, post, model_pixel, device, args.window_size_pixel, args.window_size_pixel, args.stride, args.batch_size)
    print("\nPixel registration done !\n")
    # Sub-pixel registration with correction 
    print("\nStarting the sub-pixel registration...")
    out = utils.correlation(pre, post, model_sub_pixel, device, args.window_size_sub_pixel, args.window_size_pixel, args.stride, args.batch_size, correction_maps=out_correction, correction=True)
    print("\nSub-pixel registration done !\n")
else:
    # Sub-pixel registration without correction
    print("\nStarting the sub-pixel registration...")
    out = utils.correlation(pre, post, model_sub_pixel, device, args.window_size_sub_pixel, args.window_size_sub_pixel, args.stride, args.batch_size)
    print("\nSub-pixel registration done !\n")

displacement_map_ew = out[0]
displacement_map_ns = out[1]

end_computation = time.time()
print("Displacement maps computation done !\n")

# Resize the displacement maps (if stride =/ 1, a simple Kronecker product is computed)
displacement_map_ew, displacement_map_ns = utils.resize(displacement_map_ew, displacement_map_ns, args.stride)

# Pad NaN values around the displacement maps to come back to input pair size
if args.correction:
    displacement_map_ew, displacement_map_ns = utils.padding_nan(displacement_map_ew, displacement_map_ns,
                                                               h=(args.window_size_pixel-args.stride)//2,
                                                               b=(args.window_size_pixel-args.stride)//2 + 1,
                                                               g=(args.window_size_pixel-args.stride)//2,
                                                               d=(args.window_size_pixel-args.stride)//2 + 1)
else:
    displacement_map_ew, displacement_map_ns = utils.padding_nan(displacement_map_ew, displacement_map_ns,
                                                               h=(args.window_size_sub_pixel-args.stride)//2,
                                                               b=(args.window_size_sub_pixel-args.stride)//2 + 1,
                                                               g=(args.window_size_sub_pixel-args.stride)//2,
                                                               d=(args.window_size_sub_pixel-args.stride)//2 + 1)
    
# Convert to meters and correct convention for EW
displacement_map_ew = -1 * displacement_map_ew * resolution
displacement_map_ns = displacement_map_ns * resolution

# Registering the output displacement maps with the input pair
utils.rastering_displacement_maps(displacement_map_ew, displacement_map_ns, crs, transform, stacked_maps_path)
print(f"Stacked displacement maps are stored in {displacement_maps_path}")

end = time.time()

f = open(os.path.join(folder_saving, 'infos_run.txt'), 'w')
f.write(f"Sub-pixel model: {args.model_sub_pixel_path}\n")
if args.correction:
    f.write(f"Pixel model: {args.model_pixel_path}\n")
f.write(f"Pre image: {args.pre_image}\n")
f.write(f"Post image: {args.post_image}\n")
f.write(f"Computation time : {str((end_computation - start_computation))} seconds\n")
f.write(f"Time : {str((end - start))} seconds")
f.close()    
